<?php
    include './assets/conexion.php';
    include './assets/funciones.php';
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <title>Out! - Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link
      href="https://fonts.googleapis.com/css2?family=Dosis:wght@200;500;700;800&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v6.1.1/css/all.css"
    />
    <link rel="stylesheet" href="assets/main.css" />
    <link rel="stylesheet" href="assets/normalize.css" />
    <title>Login</title>
  </head>
  <body>
    <main class="login-design">
      <div class="waves">
        <img src="assets/imagenes/loginPerson.png" alt="" />
      </div>
      <div class="login">
        <div class="login-data">
          <img src="assets/imagenes/logo-out1.png" width="235" height="235" alt="" />
          <h1>Inicio de Sesión</h1>
          <form action="login.php" method="POST" class="login-form">
            <div class="input-group">
              <label class="input-fill">
                  <input type="text" name="email" id="email" value="" onchange="this.setAttribute('value',this.value)">
                <span class="input-label">Correo Electrónico</span>
                <i class="fas fa-envelope"></i>
              </label>
            </div>
            <div class="input-group">
              <label class="input-fill">
                <input type="password" name="password" id="password" value="" onchange="this.setAttribute('value',this.value)">
                <span class="input-label">Contraseña</span>
                <i class="fas fa-lock"></i>
              </label>
                <?php if(isset($_SESSION['error_login'])): ?>
                    <div class="alerta alerta-error"><i class='fas fa-circle-exclamation'></i>
                        <label><?= ($_SESSION['error_login']) ?></label>
                    </div>
                    <?php endif; ?>
            </div>
            <a href="crearCuenta.php">¿Necesitas una Cuenta?</a>
            <input type="submit" value="Iniciar Sesión" class="btn-login" />
          </form>
          <?php borrarErrores(); ?>
        </div>
      </div>
    </main>
  </body>
</html>