<?php

$errores = array();
if(isset($_POST)){
    //Incluimos la conexion
    include './assets/conexion.php';
    //Rescatamos los elementos del formulario
    if(!isset($_SESSION)){
        session_start();
    }
    
    $nombre = isset($_POST['nombre']) ? mysqli_real_escape_string($db,$_POST['nombre']): false;
    $apellido = isset($_POST['apellido']) ? mysqli_real_escape_string($db,$_POST['apellido']): false;
    $email = isset($_POST['email']) ? mysqli_real_escape_string($db,$_POST['email']): false;
    $password = isset($_POST['password']) ? mysqli_real_escape_string($db,$_POST['password']): false;
    
    
    //validar campos
    if(!empty($nombre) && !is_numeric($nombre) && !preg_match("/[0-9]/", $nombre)){
        $nombre_valido = true;
    }else{
        $nombre_valido = false;
        $errores['nombre'] = "El nombre no es valido";
    }
    
    if(!empty($apellido) && !is_numeric($apellido) && !preg_match("/[0-9]/", $apellido)){
        $apellido_valido = true;
    }else{
        $apellido_valido = false;
        $errores['apellido'] = "El apellido no es valido";
    }
    
    if(!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)){
        $email_valido = true;
    }else{
        $email_valido = false;
        $errores['email'] = "El email no es valido";
    }
    
    if(!empty($password)){
        $password_valido = true;
    }else{
        $password_valido = false;
        $errores['password'] = "La contraseña esta vacia";
    }
    
    $guardar_usuario = false;
    
    if(count($errores)==0){
        //crear una sentencia sql que me arroje la linea del usuario
        $sql1 = "select * from usuarios where email = '$email'";
        // $consulta = ejecutamos el select
        $consulta = mysqli_query($db,$sql1);
        //condicion mysqli_num_row($consulta) == 0
        if(mysqli_num_rows($consulta) == 0){
            $guardar_usuario = true;
            //Cifrar contraseña
            //md5($strin5); sha($string)
            $password_segura = password_hash($password,PASSWORD_BCRYPT,['cost'=>4]);
            //echo $password_segura;die();
        
            //Insertar en la base de datos
            $sql = "insert into usuarios values(null,'$nombre','$apellido','$email','$password_segura',CURDATE(),0)";
            $guardar = mysqli_query($db,$sql);
        if($guardar){
            header("Location: index.php");die();
        }else{
            $_SESSION['errores']['general'] = "Existe un error en el registro";
        }    
        }else{
            $_SESSION['errores']['general'] = "Ya existe este correo";
        }
    }else{
        $_SESSION['errores'] = $errores;
    }
}
header("Location: crearCuenta.php");