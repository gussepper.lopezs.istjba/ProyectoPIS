<?php
    include './assets/conexion.php';
    include './assets/funciones.php';
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Out! - Registro</title> 
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0">
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Dosis:wght@200;500;700;800&display=swap"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          href="https://use.fontawesome.com/releases/v6.1.1/css/all.css"
        />
	<link rel="stylesheet" href="assets/main.css">
	
</head>  
<body>
     <div class="login">
        <div class="login-data">
         <h1>Registrate</h1>
        <div class="input-contenedor">
             <form action="registro.php" method="POST" class="login-form">
                <div class="input-group">
                  <label class="input-fill">
                    <input type="text" name="nombre" id="nombre" value="" onchange="this.setAttribute('value',this.value)">
                    <span class="input-label">Nombre</span>
                    <i class="fa fa-user"></i>
                  </label>
                    <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'nombre'): ''; ?>
                </div>
                <div class="input-group">
                  <label class="input-fill">
                    <input type="text" name="apellido" id="apellido" value="" onchange="this.setAttribute('value',this.value)">
                    <span class="input-label">Apellidos</span>
                    <i class="fa fa-user"></i>
                  </label>
                    <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'apellido'): ''; ?>
                </div>
                <div class="input-group">
                  <label class="input-fill">
                    <input type="text" name="email" id="email" value="" onchange="this.setAttribute('value',this.value)">
                    <span class="input-label">Correo Electrónico</span>
                    <i class="fas fa-envelope"></i>
                  </label>
                    <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'email'): ''; ?>
                     <?php if(isset($_SESSION['errores']['general'])): ?>
                    <div class="alerta alerta-error"><i class='fas fa-circle-exclamation'></i>
                        <label><?= ($_SESSION['errores']['general']) ?></label>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="input-group">
                  <label class="input-fill">
                    <input type="password" name="password" id="password" value="" onchange="this.setAttribute('value',this.value)">
                    <span class="input-label">Contraseña</span>
                    <i class="fas fa-lock"></i>
                  </label>
                    <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'password'): ''; ?>
                </div>
                <a href="index.php">¿Ya tienes una Cuenta?</a>
                <input type="submit" value="Crear Cuenta" class="btn-login">
              </form>
            <?php borrarErrores(); ?>
        </div>
        </div>
     </div>
</body>
</html>