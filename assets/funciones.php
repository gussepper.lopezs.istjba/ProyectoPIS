<?php
function mostrarError($errores,$campo){
    $alerta="";
    if(isset($errores[$campo]) && !empty($errores[$campo])){
        $alerta = "<div class='alerta alerta-error'><i class='fas fa-circle-exclamation'></i><label>.$errores[$campo].</label></div>";
    }
    return $alerta;
}

function borrarErrores(){
    $borrado = false;
    if(isset($_SESSION['errores']['general'])){
        $_SESSION['errores']['general'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['errores'])){
        $_SESSION['errores'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['error_login'])){
        $_SESSION['error_login'] = null;
        $borrado = true;
    }
}

function fotoPerfil($conexion){
    $sql = "SELECT e.*, c.ruta_img FROM usuarios as e
    inner join imagen_perfil as c on c.id = e.imagen_id;";
    
    $consulta = mysqli_query($conexion,$sql);
    $resultado = array();
    if($consulta && mysqli_num_rows($consulta)>=1){
        $resultado=$consulta;
    }
    return $resultado;
}
