<?php
    include './assets/conexion.php';
    include './assets/funciones.php';
    $profile = fotoPerfil($db);
    $profile_data = mysqli_fetch_assoc($profile);
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Out! - Social Media</title>
    <!-- ICONSCOUT CDN -->
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v2.1.6/css/unicons.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v6.1.1/css/all.css">
    <link rel="stylesheet" href="./assets/styleout.css">
</head>
<body>
    <nav>
        <div class="container">
            <h2 class="log">
                Out!
            </h2>
            <div class="search-bar">
                <i class="uil uil-search"></i>
                <input type="search" placeholder="Busca personas, grupos o temas">
            </div>
            <div class="create">
                <a href="cerrar.php"><label class="btn btn-primary">Salir</label></a>
                <div class="profile-photo">
                    <img src="./assets/img-perfiles/<?=$profile_data['ruta_img']?>" width="auto" height="auto">
                </div>
            </div>
        </div>
    </nav>
    <!---------------------------MAIN----------------------->
    <main>
        <div class="container">
            <!---------------------------LEFT----------------------->
            <div class="left">
                <a class="profile">
                    <div class="profile-photo">
                        <img src="./assets/img-perfiles/<?=$profile_data['ruta_img']?>" width="auto" height="auto">
                    </div>
                    <div class="handle">
                        <h4><?=$profile_data['nombre']?></h4>
                    </div>
                </a>
                <!-------------------------------SIDEBAR------------------------------->
                <div class="sidebar">
                    <a class="menu-item active">
                        <span><i class="uil uil-home"></i></span><h3>Inicio</h3>
                    </a>
                    <a class="menu-item" id="notifications">
                        <span><i class="uil uil-bell"><small class="notification-count">9+</small></i></span><h3>Notificaciones</h3>
                        <!-------------------------------NOTIFICATION POPUP------------------------------->
                        <div class="notifications-popup">
                            <div>
                                <div class="profile-photo">
                                    <img src="./assets/imagenes/profile-2.jpg">
                                </div>
                                <div class="notification-body">
                                    <b>Freddy Pluas</b> Acepto tu solicitud de amistad
                                    <small class="text-muted">Hace 2 dias</small>
                                </div>
                            </div>
                            <div>
                                <div class="profile-photo">
                                    <img src="./assets/imagenes/profile-3.jpg">
                                </div>
                                <div class="notification-body">
                                    <b>Tu mama</b> Comento en tu publicacion
                                    <small class="text-muted">Hace 2 horas</small>
                                </div>
                            </div>
                            <div>
                                <div class="profile-photo">
                                    <img src="./assets/imagenes/profile-4.jpg">
                                </div>
                                <div class="notification-body">
                                    <b>Sr Doe</b> Comento en tu publicacion
                                    <small class="text-muted">Hace 1 mes</small>
                                </div>
                            </div>
                            <div>
                                <div class="profile-photo">
                                    <img src="./assets/imagenes/profile-5.jpg">
                                </div>
                                <div class="notification-body">
                                    <b>Lana Rose</b> Le encanto tu publicacion
                                    <small class="text-muted">Hace 24 segundos</small>
                                </div>
                            </div>
                            <div>
                                <div class="profile-photo">
                                    <img src="./assets/imagenes/profile-6.jpg">
                                </div>
                                <div class="notification-body">
                                    <b>Donald Trump</b> Reporto tu publicacion
                                    <small class="text-muted">Hace 10 minutos</small>
                                </div>
                            </div>
                            <div>
                                <div class="profile-photo">
                                    <img src="./assets/imagenes/profile-7.jpg">
                                </div>
                                <div class="notification-body">
                                    <b>Andy manzaba</b> Le encanta tu publicacion
                                    <small class="text-muted">Hace 5 horas</small>
                                </div>
                            </div>
                        </div>
                        <!-------------------------------END NOTIFICATIONS------------------------------->
                    </a>
                    <a class="menu-item" id="messages-notification">
                        <span><i class="uil uil-envelope-alt"><small class="notification-count">6</small></i></span><h3>Mensajes</h3>
                    </a>
                    <a class="menu-item">
                        <span><i class="uil uil-users-alt"></i></span><h3>Grupos</h3>
                    </a>
                    <a class="menu-item" id="theme">
                        <span><i class="uil uil-palette"></i></span><h3>Tema</h3>
                    </a>     
                    <a class="menu-item" id="settings">
                        <span><i class="uil uil-setting"></i></span><h3>Configuración</h3>
                    </a>         
                </div>
                <!-------------------------------END OF SIDEBAR------------------------------->
                <label for="create-post" class="btn btn-primary">Empezar Publicacion</label>
            </div>
            <!-------------------------------END OF LEFT------------------------------->
            <!-------------------------------MIDDLE------------------------------->
            <div class="middle">
                <form class="create-post">
                    <div class="profile-photo">
                        <img src="./assets/img-perfiles/<?=$profile_data['ruta_img']?>" width="auto" height="auto">
                    </div>
                    <input type="text" placeholder="En que estas pensando hoy?" id="create-post">
                    <a id='upload-img'><i class="uil uil-camera-plus"></i></a>
                    <input type="submit" value="Publicar" class="btn btn-primary">
                </form>
                <!-------------------------------FEEDS------------------------------->
                <div class="feeds">
                <!-------------------------------FEED 1------------------------------->  
                    <div class="feed">
                        <div class="head">
                            <div class="user">
                                <div class="profile-photo">
                                    <img src="./assets/imagenes/profile-13.jpg">
                                </div>
                                <div class="ingo">
                                    <h3>Lana Rose</h3>
                                    <small class="text-muted">Hace 15 minutos</small>
                                </div>
                            </div>
                            <span class="edit">
                                <i class="uil uil-ellipsis-h"></i>
                            </span>
                        </div>
                        <div class="feed-text">
                            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                                ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
                                dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
                                non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h3>
                        </div>
                        <div class="photo">
                            <img src="./assets/imagenes/feed-2.jpg">
                        </div>
                        <div class="action-buttons">
                            <div class="interaction-buttons">
                                <span onclick="javascript:CambiarEstilo();"><i id="heart" class="far fa-heart"></i></span>
                                <span><i class="uil uil-comment-dots"></i></span>
                                <span><i class="uil uil-share-alt"></i></span>
                            </div>
                        </div>
                        <div class="liked-by">
                            <span><img src="./assets/imagenes/profile-10.jpg"></span>
                            <span><img src="./assets/imagenes/profile-4.jpg"></span>
                            <span><img src="./assets/imagenes/profile-15.jpg"></span>
                            <p>A <b>Donald Trump</b> le gusto y a otras <b>20 personas mas</b></p>
                        </div>
                        <div class="caption">
                            <p><b>Lana rose</b> Lorem ipsum dolor sit quisquam eius. 
                                <span class="hash-tag">#lifestyle</span></p>
                        </div>
                        <div class="comments text-muted">Ver otros 30 comentarios</div>
                    </div>
                <!-------------------------------END OF FEED 1------------------------------->  
                </div>
                <!-------------------------------END OF FEEDS------------------------------->                
            </div>
            <!-------------------------------END OF MIDDLE------------------------------->
            <!-------------------------------RIGHT------------------------------->
            <div class="right">
                <!-------------------------------MESSAGES------------------------------->
                <div class="messages">
                    <div class="heading">
                        <h4>Mensajes</h4><i class="uil uil-edit"></i>
                    </div>
                    <!-------------------------------SEARCH BAR------------------------------->
                    <div class="search-bar">
                        <i class="uil uil-search"></i>
                        <input type="search" placeholder="Buscar mensajes" id="message-search">
                    </div>
                    <!-------------------------------MESSAGES CATEGORY------------------------------->
                    <div class="category">
                        <h6 class="active">Principal</h6>
                        <h6>General</h6>
                        <h6 class="message-requests">Peticiones(7)</h6>
                    </div>
                    <!-------------------------------MESSAGE------------------------------->
                    <div class="message">
                        <div class="profile-photo">
                            <img src="./assets/imagenes/profile-3.jpg">
                        </div>
                        <div class="message-body">
                            <h5>Ama Florida</h5>
                            <p class="text-bold">2 nuevos mensajes</p>
                        </div>
                    </div>
                    <!-------------------------------END OF MESSAGE------------------------------->
                    <!-------------------------------MESSAGE------------------------------->
                    <div class="message">
                        <div class="profile-photo">
                            <img src="./assets/imagenes/profile-5.jpg">
                        </div>
                        <div class="message-body">
                            <h5>Ana frank</h5>
                            <p class="text-muted">Hola! primer mensaje</p>
                        </div>
                    </div>
                    <!-------------------------------END OF MESSAGE------------------------------->
                    <!-------------------------------MESSAGE------------------------------->
                    <div class="message">
                        <div class="profile-photo">
                            <img src="./assets/imagenes/profile-13.jpg">
                            <div class="active"></div>
                        </div>
                        <div class="message-body">
                            <h5>Bart Edinson</h5>
                            <p class="text-bold">NO</p>
                        </div>
                    </div>
                    <!-------------------------------END OF MESSAGE------------------------------->
                    <!-------------------------------MESSAGE------------------------------->
                    <div class="message">
                        <div class="profile-photo">
                            <img src="./assets/imagenes/profile-16.jpg">
                        </div>
                        <div class="message-body">
                            <h5>Michael Jackson</h5>
                            <p class="text-bold">Hee Hee</p>
                        </div>
                    </div>
                    <!-------------------------------END OF MESSAGE------------------------------->
                    <!-------------------------------MESSAGE------------------------------->
                    <div class="message">
                        <div class="profile-photo">
                            <img src="./assets/imagenes/profile-19.jpg">
                            <div class="active"></div>
                        </div>
                        <div class="message-body">
                            <h5>Freddy Pluas</h5>
                            <p class="text-muted">oeoeoeoeoe</p>
                        </div>
                    </div>
                    <!-------------------------------END OF MESSAGE------------------------------->
                    <!-------------------------------MESSAGE------------------------------->
                    <div class="message">
                        <div class="profile-photo">
                            <img src="./assets/imagenes/profile-20.jpg">
                        </div>
                        <div class="message-body">
                            <h5>Andy vera</h5>
                            <p class="text-bold">jajajajajaj</p>
                        </div>
                    </div>
                    <!-------------------------------END OF MESSAGE------------------------------->
                </div>
                <!-------------------------------END OF MESSAGES------------------------------->
                <!-------------------------------FRIEND REQUEST------------------------------->
                <div class="friend-request">
                    <h4>Solicitudes</h4>
                    <!-------------------------------REQUEST------------------------------->
                    <div class="request">
                        <div class="info">
                            <div class="profile-photo">
                                <img src="./assets/imagenes/profile-11.jpg">
                            </div>
                            <div>
                                <h5>Tomas Watson</h5>
                                <p class="text-muted">
                                    5 amigos en comun
                                <p>
                            </div>
                        </div>
                        <div class="action">
                            <button class="btn btn-primary">Aceptar</button>
                            <button class="btn">Declinar</button>
                        </div>
                    </div>
                    <!-------------------------------END OF REQUEST------------------------------->
                    <!-------------------------------REQUEST------------------------------->
                    <div class="request">
                        <div class="info">
                            <div class="profile-photo">
                                <img src="./assets/imagenes/profile-18.jpg">
                            </div>
                            <div>
                                <h5>Miles Morales</h5>
                                <p class="text-muted">
                                    1 amigo en comun
                                <p>
                            </div>
                        </div>
                        <div class="action">
                            <button class="btn btn-primary">Aceptar</button>
                            <button class="btn">Declinar</button>
                        </div>
                    </div>
                    <!-------------------------------END OF REQUEST------------------------------->
                    <!-------------------------------REQUEST------------------------------->
                    <div class="request">
                        <div class="info">
                            <div class="profile-photo">
                                <img src="./assets/imagenes/profile-20.jpg">
                            </div>
                            <div>
                                <h5>Andrea Holmes</h5>
                                <p class="text-muted">
                                    20 amigos en comun
                                <p>
                            </div>
                        </div>
                        <div class="action">
                            <button class="btn btn-primary">Aceptar</button>
                            <button class="btn">Declinar</button>
                        </div>
                    </div>
                    <!-------------------------------END OF REQUEST------------------------------->
                </div>
                <!-------------------------------END OF FRIEND REQUESTS------------------------------->
            </div>
            <!-------------------------------END OF RIGHT------------------------------->
        </div>
    </main>
    <!--------------------------------------------------------------THEME CUSTOMIZATION-------------------------------------------------------------->
    <div class="customize-theme">
        <div class="card">
            <h2>Personaliza tu entorno</h2>
            <p class="text-muted">Personaliza el tamaño de la fuente, el color y el fondo</p>
            <!-------------------------------FONT SIZES------------------------------->
            <div class="font-size">
                <h4>Tamaño de fuente</h4>
                <div>
                    <h6>Aa</h6>
                <div class="choose-size">
                    <span class="font-size-1"></span>
                    <span class="font-size-2"></span>
                    <span class="font-size-3 active"></span>
                    <span class="font-size-4"></span>
                    <span class="font-size-5"></span>
                </div>
                <h3>Aa</h3>
                </div>
            </div>
            <!-------------------------------PRIMARY COLORS------------------------------->
            <div class="color">
                <h4>Color</h4>
                <div class="choose-color">
                    <span class="color-1 active"></span>
                    <span class="color-2"></span>
                    <span class="color-3"></span>
                    <span class="color-4"></span>
                    <span class="color-5"></span>
                </div>
            </div>
            <!-------------------------------BACKGROUND COLORS------------------------------->
            <div class="background">
                <h4>Fondo</h4>
                <div class="choose-bg">
                    <div class="bg-1 active">
                        <span></span>
                        <h5 for="bg-1">Claro</h5>
                    </div>
                    <div class="bg-2">
                        <span></span>
                        <h5>Semi Oscuro</h5>
                    </div>
                    <div class="bg-3">
                        <span></span>
                        <h5 for="bg-3">Oscuro</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <!--------------------------------------------------------------CONFIGURATION-------------------------------------------------------------->
    <div class="configuration">
        <div class="card">
            <h2>Configura tu perfil</h2>
            <p class="text-muted">Personaliza tus datos de perfil</p>
            <form action="subir.php" method="POST" enctype="multipart/form-data">
                <div class="pic-group">    
                    <div class="profile-photo">
                        <img src="./assets/img-perfiles/<?=$profile_data['ruta_img']?>" width="auto" height="auto">
                    </div>
                    <label class="btn btn-primary" for="upload">
                    <input id="upload" type="file" name="archivo">
                    <span>Subir</span>
                    </label>
                </div>
                <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'tipo'): ''; ?>
                <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'tamano'): ''; ?>
                <div class="input-group">
                    <h4>Nombre:</h4>
                    <input type="text" name="nombre" id="nombre" value="<?=$profile_data['nombre']?>">
                </div>
                <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'nombre'): ''; ?>
                <div class="input-group">
                    <h4>Apellidos:</h4>
                    <input type="text" name="apellido" id="apellido" value="<?=$profile_data['apellido']?>">
                </div>
                <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'apellido'): ''; ?>
                <?php if(isset($_SESSION['errores']['general'])): ?>
                    <div class="alerta alerta-error"><i class='fas fa-circle-exclamation'></i>
                        <label><?= ($_SESSION['errores']['general']) ?></label>
                    </div>
                    <?php endif; ?>
                <input type="submit" value="Guardar" class="btn btn-primary">
            </form>
        </div>
    </div>
    
<script src="./assets/principal.js"></script>
</body>
</html>